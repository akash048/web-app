export const signupValidationSchema = {
    username: {
        required: {
            errorMsg: `Username is required`
        }
    },
    email: {
        required: {
            errorMsg: `Email is required`
        },
        email: {
            errorMsg: `Enter a valid email`
        }
    },
    password: {
        required: {
            errorMsg: `Password is required`
        }
    },
    confirm_password: {
        required: {
            errorMsg: `Confirm password is required`
        }
    }
  }