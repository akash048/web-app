export const loginValidationSchema = {
    username: {
        required: {
            errorMsg: `Username is required`
        }
    },
    password: {
        required: {
            errorMsg: `Password is required`
        }
    }
  }