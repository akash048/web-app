import React from 'react';
import  Routes  from './Component/Routes/Routes';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';

function App() {
  return (
    <div className="App">
      <Routes />
    </div>
  );
}

export default App;
