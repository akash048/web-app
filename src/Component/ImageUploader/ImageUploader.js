import React from 'react'
import Dropzone from 'react-dropzone'
import './ImageUploader.css';

const ImageUploader = ({ children, onDrop, loading }) => {
  return (
    <Dropzone 
      className = 'dropzone'
      onDrop={onDrop}
      accept={[ '.jpg', '.jpeg', '.png' ]}
      multiple
    >
      {({ getRootProps, getInputProps }) => (
        <div {...getRootProps()} className='dropzone'>
          <input {...getInputProps()} disabled={loading} />
          {children}
        </div>
      )}
    </Dropzone>
  )
}
export default ImageUploader