import React, { Component } from "react";
import { Row, Col } from "react-bootstrap"
import microValidator from 'micro-validator'
import is from 'is_js'
import axios from 'axios'

import './Login.css'
import { loginValidationSchema } from '../../Validation/LoginValidation';
import Navbar from "../NavBar/NavBar";
import { API_URL } from '../../Constant/Constant'


class Login extends Component {

  state = {

    userData: {
      username: '',
      password: ''
    },

    loginError: {
     
    },
    invalidLogin : ''
  }

  // let [userData, setUserData] = useState(
  //   {
  //     username: '',
  //     password: ''
  //   }
  // )
  // let [loginError, setLoginError] = useState()
  // let [serverError, setServerError] = useState('')

  // let handleChange = (e) => {
  //   const { name, value } = e.target
  //   setUserData({
  //     ...userData,
  //     [name]: value
  //   })
  // }

  handleChange = (e) => {
    const { name, value } = e.target
    let {userData} = this.state
    userData[name] = value
    this.setState({userData})
  }
  
  handleSubmit = () => {
    const error = microValidator.validate(loginValidationSchema, this.state.userData)
    if (!is.empty(error)) {
      this.setState({
        loginError: error
      })
      return
    }
    let payload = {
      user: this.state.userData.username,
      password: this.state.userData.password
    }
    axios.post(`${API_URL}users/SignInAdmin`, payload)
    .then(res => {
      if(res.data.status){
        localStorage.setItem('username', this.state.userData.username)
        localStorage.setItem('isLoggedIn', true)
        this.props.history.push('/Companies')
      }
    })
    .catch(err => {
      this.setState({
        invalidLogin: 'Invalid login Details'
      })
    })
  }

  render() {
    console.log("api url -----------------------------==========- >", API_URL)
    if (localStorage.isLoggedIn) {
      let param = 'Companies'
      this.props.history.push(`/${param}`)
    }
    return (
      <div>
        {/* <Navbar /> */}
        <div className='container'>
          <Row>
            <Col sm={{ span: 6, offset: 3 }}>
              <div className="loginForm">
                <div className="innerContainer">
                  <h3>Sign In</h3>
                  <div className='form-container'>
                    <div className="form-group label">
                      <input name="username" onChange={(e) => this.handleChange(e)} type="text" className="form-control" placeholder="Enter username" />
                      <p className='error'>{this.state.loginError.username}</p>
                    </div>
                    <div className="form-group label">
                      <input name="password" onChange={(e) => this.handleChange(e)} type="password" className="form-control" placeholder="Enter password" />
                      <p className='error'>{this.state.loginError.password}</p>
                    </div>
                    <button onClick={this.handleSubmit} type="submit" className="btn btn-primary btn-block">Submit</button>
                    <p className='error'>{this.state.invalidLogin}</p>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}
export default Login;
