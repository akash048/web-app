import React, { Component } from 'react';
import '../../App.css';
import './Home.css';
import SideBar from '../SideBar/SideBar'
import Heading from '../Heading/Heading'
import Table from '../Table/Table'
import Footer from '../Footer/Footer'
import 'font-awesome/css/font-awesome.min.css';
import '../../Css/admin.css';
import '../../Css/bootstrap4.min.css';
import Loader from '../Loader/Loader'

class Home extends Component {
  state = { isLoader: true }
  callbackFunction = (childData) => {
    this.setState({ isLoader: childData })
  }
  render() {
    return (
      <div id="page-top">
        {this.state.isLoader? <Loader /> : null}
        <div id="wrapper">
          <SideBar props={this.props} />
          <div id="content-wrapper" className="d-flex flex-column">
            <Heading props={this.props}/>
            {/* <Spinner animation="border" role="status">
              <span className="sr-only">Loading...</span>
            </Spinner> */}
            <Table parentCallback = {this.callbackFunction} props={this.props} />
            <Footer />
          </div>
        </div>
      </div>
    );
  }
}
export default Home;