import React,  {Component} from 'react';
import { BrowserRouter, Switch, Route,  Redirect} from "react-router-dom";
import  Home  from '../Home/Home';
import  Login  from '../Login/Login';

class Routes extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Switch>
            <Route exact path='/sign-in' component={Login} />
            <Route exact path='/' component={Login} />
            <AuthRoute exact path='/:id' component={Home} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

const AuthRoute = ({ component: Component, ...rest }) => {
  const isLoggedIn = localStorage.isLoggedIn
  return (
    <Route
      {...rest}
      render={props =>
        isLoggedIn ? (
          <Component {...props}/>
        ) : (
          <Redirect to={{ pathname: '/sign-in'}} />
        )
      }
    />
  )
}

export default Routes;