import React, {Fragment} from "react";
import axios from "axios";
import moment from 'moment'
import { Modal, Row, Col } from 'react-bootstrap';

import ImageUploader from '../ImageUploader/ImageUploader'
import './ItemPreviewModal.css';
import { API_URL } from '../../Constant/Constant'


class ModalUI extends React.Component {
    state = {
        Images : [],
        Itemimages:[],
        values: [
            { name: 'New', id: 1 },
            { name: 'Visible', id: 2 },
            { name: 'Not Visible', id: 3 },
          ],
          mainCheckbox : {
              active:false,
              index:null
          },
        data: {
            Id: null,
            ItemId: null,
            FileUrl: '',
            Main : true,
            CreatedBy: 1,
            ModifiedBy : 1
        }
    }

    // componentDidMount(){
    //     console.log("component did mount")
    //     axios.get(`${API_URL}misc/getitempictures/${this.props.state.itemImageId}`)
    //     .then(res => {
    //         console.log("res . data . data ---->>>", res.data.data)
    //       this.setState({
    //         ItemImages: res.data.data,
    //       })
    //     })
    //   }
      
    componentWillReceiveProps(nextProps) {
        console.log("component will mount")
        this.setState({ Images: nextProps.state.imageNames })
    }

    onDropImage = (files) => {
        this.props.saveImage(files, this.props.state.itemData.id, false)
    }
    removeImage = (image, index) => {
        var updatedImages = this.state.Images.filter(img => {
            return img !== image
        });
        this.setState({
            Images: updatedImages
        })
    }

    handleCheckboxClick = (index, val, state) => {
        this.markMain(index, val, state)
        if(index === this.state.mainCheckbox.index || !this.state.mainCheckbox.index){
            this.setState({
                mainCheckbox: {
                    active:!this.state.mainCheckbox.active,
                    index: index
                }
            })
        }else{
            this.setState({
                mainCheckbox: {
                    active:this.state.mainCheckbox.index,
                    index: index
                }
            })
        }
    }

    closeModal = () => {
        
    }

    markMain = (index, val, state) => {
        this.setState({
            data: {
                Id: val.id,
                ItemId: val.itemId,
                FileUrl: val.imageUrl,
                Main : true,
                CreatedBy : 1,
                ModifiedBy : 1
            }
        })
    
    }
    // deleteItemImage = (val) =>{
    //     console.log("val -=>", val)
    //     console.log("api -=>", `${API_URL}misc/deleteitempicture/${val.id}/${val.createdBy}`)
    //     axios.delete(`${API_URL}misc/deleteitempicture/${val.id}/${val.createdBy}`).
    //     then(res =>{
    //         console.log("res -=>" , res)
    //     })
    // }

    closeModal = () =>{
        let {handleItemModalClose} = this.props
        this.setState({
            mainCheckbox: {
                active:false,
                index: null
            }
        })
        handleItemModalClose()
    }

     saveItemPreview =  () =>{
        let formData = new FormData()
        formData.append("Id", this.state.data.Id);
        formData.append("FileUrl", this.state.data.FileUrl);
        formData.append("ItemId", this.state.data.ItemId);
        formData.append("Main", true);
        formData.append("CreatedBy", 1);
        formData.append("ModifiedBy", 1);
        axios.post(`https://bimiscwebapitest.azurewebsites.net/api/misc/saveitempicture`, formData)
          .then(res => {
            this.closeModal()
          })
    }

    render() {
        let {state} = this.props
        let { handleNameChange} = this.props
        let { handleRecordChange} = this.props
        let {saveMultipe} = this.props
        let { editRow } = this.props
       
        return (
          <div>
            <Modal
              size="lg"
              show={state.ItemModalshow}
              onHide={this.props.handleItemModalClose}
            >
              <Fragment>
                <div>
                  <div className="modal-header border-0 bg-primary">
                    <h3 className="modal-title text-white">Item Record</h3>
                    <div>
                    <button
                        onClick={this.props.handleItemModalClose}
                        type="button"
                        className="btn btn-primary"
                        data-dismiss="modal"
                      >
                          <h3 onClick = {() => this.saveItemPreview()}>
                          Save
                          </h3>
                        
                      </button>
                    <button
                        onClick={this.props.handleItemModalClose}
                        type="button"
                        className="btn btn-primary"
                        data-dismiss="modal"
                      ><h3>
                        &times;
                      </h3>
                        
                      </button>
                      
                      
                    </div>
                  </div>
                  <div className="headingContainer">
                    <div>
                      <h4>Image Preview</h4>
                    </div>
                    <div>
                      <ImageUploader onDrop={this.onDropImage}>
                        <i class="fas fa-plus fa-2x"></i>
                      </ImageUploader>
                    </div>
                  </div>
                  <div className="modal-body center">
                    <Row className="imageOut">
                      {this.props.state.itemImages.map((val, index) => {
                        return (
                          <Col
                            xs={4}
                            md={2}
                            sm={3}
                            key={index}
                            className="imagePreviewOut"
                          >
                            <img
                              className="imageMainPreview"
                              src={val.imageUrl}
                              alt="img"
                            />
                            <div className="displayButtons">
                              <div className="preview-tools"></div>
                              <div className="toolsButton">
                                <i
                                  onClick={() =>
                                    this.props.deleteItemImage(
                                      val,
                                      state.itemData.id
                                    )
                                  }
                                  className="fas fa-trash-alt fa-2x text-warningn main-check"
                                ></i>

                                <div className="input-checkbox">
                                  <input
                                    type="checkbox"
                                    checked={
                                      this.state.mainCheckbox.index === index &&
                                      this.state.mainCheckbox.active
                                        ? true
                                        : false
                                    }
                                    onChange={() =>
                                      this.handleCheckboxClick(index, val, state)
                                    }
                                  />
                                  <label>Mark Main</label>
                                </div>
                              </div>
                            </div>
                          </Col>
                        );
                      })}
                    </Row>
                    <table
                      className="table table-bordered"
                      id="dataTable"
                      width="100%"
                      cellSpacing="0"
                    >
                      <thead>
                        <tr>
                          <th>Item Preview</th>
                          <th>ID</th>
                          <th>Name</th>
                          <th>Record Status</th>
                          <th>Created By User</th>
                          <th>Date Created</th>
                          <th>Modified User</th>
                          <th>Date Modified</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <img
                              className="item-preview"
                              src={state.itemData.thumbImageUrl}
                              alt="img"
                            ></img>
                          </td>
                          <td>{state.itemData.id}</td>
                          <td>
                            <input
                              className={state.itemEditable ? "" : "input_cell"}
                              type="text"
                              value={state.itemData.name}
                              onChange={e =>
                                handleNameChange(state.itemIndex, e)
                              }
                              disabled={state.itemEditable ? "" : "disabled"}
                            />
                          </td>
                          <td>
                            {state.itemEditable ? (
                              <select
                                value={this.state.value}
                                onChange={e => {
                                  handleRecordChange(state.itemIndex, e);
                                }}
                                className="custom-select w-100"
                              >
                                {this.state.values.map((val, i) => {
                                  if (
                                    val.id === state.itemData.recordStatusId
                                  ) {
                                    return (
                                      <option selected={val.id}>
                                        {val.name}
                                      </option>
                                    );
                                  }
                                  return (
                                    <option value={val.id}>{val.name}</option>
                                  );
                                })}
                              </select>
                            ) : (
                              <Fragment>{state.itemData.recordStatus}</Fragment>
                            )}
                          </td>
                          <td>{state.itemData.createdByUser}</td>
                          <td>{moment(state.data.dateCreated).format("L")}</td>
                          <td>{state.itemData.modifiedByUser}</td>
                          <td>
                            {moment(state.itemData.dateModified).format("L")}
                          </td>
                          {state.itemEditable ? (
                            <td onClick={() => saveMultipe()}>
                              <i class="fas fa-save fa-2x text-success"></i>
                            </td>
                          ) : (
                            <td onClick={() => editRow()}>
                              <i className="fas fa-pen text-warning"></i>{" "}
                            </td>
                          )}
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </Fragment>
            </Modal>
          </div>
        );
    }
}

export default ModalUI
