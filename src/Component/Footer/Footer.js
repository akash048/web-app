import React, { Component } from 'react';
import 'font-awesome/css/font-awesome.min.css';
import '../../Css/admin.css';
import '../../Css/bootstrap4.min.css';


class Footer extends Component {
    render() {
        return (
            <div>
                <footer className="sticky-footer bg-white">
                    <div className="container my-auto">
                        <div className="copyright text-center my-auto">
                            <span>Copyright &copy; Web App 2019</span>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}

export default Footer;