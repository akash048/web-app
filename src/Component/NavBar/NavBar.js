import React, { useState } from 'react';
import {Navbar, NavDropdown, Nav, NavItem, Form, FormControl, Button} from 'react-bootstrap';

const NavBar = (props) => {

  return (
    <div>
       <Navbar bg="primary" expand="lg">
        <Navbar.Brand href="#home">React-Seed</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
                <Nav.Link href="#home">Home</Nav.Link>
                <Nav.Link href="#home">Login</Nav.Link>
                <Nav.Link href="#home">Profile</Nav.Link>
                <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                    <NavDropdown.Item href="#action/3.1">Option1</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.2">Option2</NavDropdown.Item>
                </NavDropdown>
            </Nav>
        </Navbar.Collapse>
        </Navbar>
    </div>
  );
}

export default NavBar;