import React, { Fragment } from "react";
import { Modal } from 'react-bootstrap';

import ImageUploader from '../ImageUploader/ImageUploader'
import './Modal.css';

class ModalUI extends React.Component {

    state = {
        Images: []
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ Images: nextProps.state.imageNames })
    }

    onDropImage = (files) => {
        if (!files.length) {
            return
        }
        if (files.length > 1) {
            this.setState({
                Images: files
            })
            // for (let i = 0; i < this.state.Images.length; i++) {
            // }
        } else {
            // let formData = new FormData()
            // formData.append('FileUrl', files[0])
            // axios.post(`${api_url}/fileupload`, formData)
            // .then(res => { })
            this.setState({
                Images: [
                    ...this.state.Images,
                    files[0]
                ]
            })
        }
    }

    removeImage = (image, index) => {
        var updatedImages = this.state.Images.filter(img => {
            return img !== image
        });
        this.setState({
            Images: updatedImages
        })
    }

    render() {
        let optionTemplate = this.props.values.map((v, i) => {
            if (i + 1 === this.props.selectedValue) {
                return (<option selected={v.id} value ={v.id} key={i}>{v.name}</option>)
            }
            return (<option value={v.id} key={i}>{v.name}</option>)
        });

        return (
            <div>
                {/* <Loader></Loader> */}
                <Modal show={this.props.show} onHide={this.props.handleClose}>
                    {this.props.modalType === 0 ?
                        <Fragment>
                            <div className="modal-header border-0 bg-primary">
                                <h4 className="modal-title text-white">Add Records</h4>
                                <button onClick={this.props.handleClose} type="button" className="close text-white" data-dismiss="modal">&times;</button>
                            </div>
                            <div className="modal-body">
                                {this.props.state.selectedTab !== 'CompanyAddress' ?
                                    <div className=" w-100 px-2 py-3">
                                        <label htmlFor="name">Name</label>&nbsp;&nbsp;&nbsp;
                                    <input defaultValue={this.props.editData.Name} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="name" placeholder="Enter name" name="Name" />
                                        <p className="error-message">{this.props.errors.Name ? (this.props.errors.Name[0]) : ''}</p>
                                        <p className="error-message">{this.props.submitError === '' ? '' : this.props.submitError}</p>
                                    </div>
                                    : null}


                                <div className=" w-100 px-2 py-3">
                                    <label htmlFor="records">Record Status</label>&nbsp;&nbsp;&nbsp;
					                <select name="RecordStatusId" onChange={(e) => { this.props.handleFieldChange(e) }} className="custom-select w-100">
                                    {/* <select name="RecordStatusId"  onChange={(e) => { this.test(e) }} className="custom-select w-100">   */}
                                        {optionTemplate}
                                    </select>
                                </div>

                                {
                                    this.props.state.selectedTab === 'CompanyAddress' ?
                                        <Fragment>
                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="companyId">CompanyId</label>&nbsp;&nbsp;&nbsp;
                                                <input type="hidden" defaultValue={this.props.editData.CompanyId} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="qbid" placeholder="Enter CompanyId" name="CompanyId" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="type">Type</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.Type} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="type" placeholder="Enter Type" name="Type" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="address">Address</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.Address} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="Address" placeholder="Enter Address" name="Address" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="city">City</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.City} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="city" placeholder="Enter City" name="City" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="province">Province</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.Province} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="Province" placeholder="Enter Province" name="Province" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="postalCode">PostalCode</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.PostalCode} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="city" placeholder="Enter PostalCode" name="PostalCode" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="country">Country</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.Country} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="country" placeholder="Enter Country" name="Country" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="contactPerson">ContactPerson</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.ContactPerson} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="contactPerson" placeholder="Enter ContactPerson" name="ContactPerson" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="email">Email</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.Email} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="Email" placeholder="Enter Email" name="Email" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="phone">Phone</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.Phone} onChange={(e) => { this.props.handleFieldChange(e) }} type="number" className="form-control w-100" id="phone" placeholder="Enter Phone" name="Phone" max="10" />
                                            </div>
                                        </Fragment> :
                                        null
                                }


                                {
                                    this.props.state.selectedTab === 'Companies' ?
                                        <Fragment>
                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="records">QbId</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.QbId} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="qbid" placeholder="Enter qbid" name="QbId" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="records">IndustryId</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.IndustryId} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="IndustryId" placeholder="Enter IndustryId" name="IndustryId" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="records">WebSite</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.WebSite} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="website" placeholder="Enter website" name="WebSite" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="records">Phone</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.Phone} onChange={(e) => { this.props.handleFieldChange(e) }} type="number" className="form-control w-100" id="phone" placeholder="Enter phone" name="Phone" maxLength="10" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">

                                                <label htmlFor="records">Active</label>&nbsp;&nbsp;&nbsp;
                                                <select name="Active"  onChange={(e) => { this.props.handleFieldChange(e) }}
                                                    className="custom-select w-100">
                                                    {
                                                        this.props.state.ActiveVal.map((val, i) => {
                                                            console.log("conpny cal  --------== =--=>", val)
                                                            if (val == this.props.editData.Active) {
                                                                return (<option selected={val.toString()}>{this.props.editData.Active.toString()}</option>)
                                                            }
                                                            return (<option value={val}>{val.toString()}</option>)
                                                        })
                                                    }
                                                </select>
                                            </div>
                                        </Fragment> :
                                        null
                                }



                                {
                                    this.props.state.selectedTab === 'Projects' ?
                                        <Fragment>
                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="address">Address</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.Address} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="address" placeholder="Enter Address" name="Address" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="city">City</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.City} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="city" placeholder="Enter City" name="City" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="province">Province</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.Province} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="province" placeholder="Enter Province" name="Province" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="postalCode">PostalCode</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.PostalCode} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="postalCode" placeholder="Enter PostalCode" name="PostalCode" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="country">Country</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.Country} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="country" placeholder="Enter Country" name="Country" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="startDate">StartDate</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.StartDate} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="startDate" placeholder="Enter StartDate" name="StartDate" />
                                            </div>


                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="endDate">EndDate</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.EndDate} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="startDate" placeholder="Enter EndDate" name="EndDate" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="buildingTypeId">BuildingTypeId</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.BuildingTypeId} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="buildingTypeId" placeholder="Enter BuildingTypeId" name="BuildingTypeId" />
                                            </div>

                                            <div className=" w-100 px-2 py-3">
                                                <label htmlFor="contractorId">ContractorId</label>&nbsp;&nbsp;&nbsp;
                                                <input defaultValue={this.props.editData.ContractorId} onChange={(e) => { this.props.handleFieldChange(e) }} type="text" className="form-control w-100" id="contractorId" placeholder="Enter ContractorId" name="ContractorId" />
                                            </div>



                                            <div className=" w-100 px-2 py-3">

                                                <label htmlFor="records">Active</label>&nbsp;&nbsp;&nbsp;
                                                    <select name="Active" value={this.props.value} onChange={(e) => { this.props.handleFieldChange(e) }} className="custom-select w-100">
                                                    <option>true</option>
                                                    <option>false</option>
                                                </select>
                                            </div>
                                        </Fragment> :
                                        null
                                }

                                <div>
                                    {this.props.state.selectedTab === 'Items' ?
                                        <ImageUploader onDrop={this.onDropImage}>
                                            <div className="image-uploader">
                                                <i class="fas fa-paperclip"></i> {'\u00A0'}  Upload Image
                                            </div>
                                        </ImageUploader>
                                        :
                                        null
                                    }
                                </div>
                                {this.state.Images.map((image, index) => {
                                    return (
                                        <div className='show-image-names' >
                                            <p>{this.state.Images[index].name}</p>
                                            <i onClick={() => this.removeImage(image, index)} class="fas fa-times"></i>
                                        </div>
                                    )
                                })}

                                <div className="d-flex w-100 px-2 py-3">
                                    <button onClick={() => this.props.handleSubmit(this.state.Images)} type="submit" className="btn btn-primary">Submit</button>&nbsp;&nbsp;
					                <button onClick={this.props.handleClose} data-dismiss="modal" className="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </Fragment>
                        :
                        <Fragment>
                            <div>
                                <div className="modal-header border-0 bg-primary">
                                    <h4 className="modal-title text-white">Delete Record</h4>
                                    <button onClick={this.props.handleClose} type="button" className="close text-white" data-dismiss="modal">&times;</button>
                                </div>
                                <div className="modal-body center">
                                    <h3>Are you sure ?</h3>
                                    <div className="d-flex w-100 px-2 py-3 buttonOut">
                                        <button onClick={this.props.deleteRow} type="submit" className="btn btn-primary">Yes</button>&nbsp;&nbsp;
					                    <button onClick={this.props.handleClose} data-dismiss="modal" className="btn btn-danger">No</button>
                                    </div>
                                    <p className="error-message" >{this.props.state.errors.message}</p>
                                </div>
                            </div>
                        </Fragment>
                    }
                </Modal>
            </div>
        );
    }
}

export default ModalUI
