import React from "react";
import { Form, Button, Modal } from 'react-bootstrap';
import './Modal.css';

class ModalUI extends React.Component {

    handleNameChange = (e) => {
      console.log("edit =>", this.props.editData)
      };
    

  render() {
    return (
      <div>
        <Button variant="primary" onClick={this.props.handleShow}>
        Add New
      </Button>
      <Modal show={this.props.show}  onHide={this.props.handleClose} animation={false}>
        <Modal.Header closeButton>
          <Modal.Title>Title</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form>
            <Form.Group controlId="formBasicPassword">
                <Form.Label>Name</Form.Label>
                <Form.Control  defaultValue = {this.props.editData.Name} type="text"  onChange={(e)=>{this.handleNameChange(e)}} />
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
                <Form.Label>RecordStatusId</Form.Label>
                <Form.Control defaultValue = {this.props.editData.RecordStatusId} type="text" />
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
                <Form.Label>ModifiedBy</Form.Label>
                <Form.Control defaultValue = {this.props.editData.ModifiedBy} type="text"  />
            </Form.Group>        
            <Button variant="primary" type="submit">
                Submit
            </Button>
        </Form>
        </Modal.Body>
      </Modal>      
      </div>
    );
  }
}

export default ModalUI
