import React, { Component, Fragment } from 'react';
import axios from "axios";
import moment from 'moment'
import microValidator from 'micro-validator'
import is from 'is_js'
import { Typeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';

import Modal from '../Modal/Modal'
import Loader from '../Loader/Loader'
import ItemPreviewModal from '../ItemPreviewModal/ItemPreviewModal'
import Pagination from '../Pagination/Pagination'
import { API_URL } from '../../Constant/Constant'
import { validationSchema } from '../Validation/Validation';
import './Table.css'
import '../../App.css';
import '../../Css/admin.css';
import '../../Css/bootstrap4.min.css';
import 'font-awesome/css/font-awesome.min.css';
import { array, bool } from 'prop-types';



let MultiEditarray = []
let payload
class Table extends Component {
  state = {
    isPagination: true,
    selectedTab: this.props.props.match.params.id,
    tableHeading: [],
    tableData: [],
    data: [],
    BuildingTypeData: [],
    contractorData: [],
    buildingTypeIndex: 1,
    itemData: {},
    itemImages: [],
    itemImageId: null,
    itemIndex: null,
    imageNames: [],
    updatedData: [],
    bulkEdit: [],
    errors: {},
    editData: {
      Id: 0,
      Name: '',
      RecordStatusId: 1,
      DateCreated: new Date(),
      QbId: null,
      WebSite: '',
      Phone: '',
      IndustryId: null,
      Active: Boolean,
      activeComapnyId: 2,
      Address: '',
      ShowAddress: false,
      City: '',
      Province: '',
      PostalCode: '',
      Country: '',
      StartDate: null,
      EndDate: null,
      BuildingTypeId: null,
      ContractorId: null,
      Type: '',
      ContactPerson: '',
      Email: '',
      Phone: '',
      CompanyId: '',
    },
    deleteData: {
      index: null,
      recordId: '',
      userId: ''
    },
    values: [
      { name: 'New', id: 1 },
      { name: 'Visible', id: 2 },
      { name: 'Not Visible', id: 3 },
    ],
    ActiveVal: [
      true,
      false
    ],
    get: '',
    save: '',
    savePicture: '',
    delete: '',
    data_length: '',
    selectedValue: 1,
    show: false,
    ItemModalshow: false,
    activePageNumber: 1,
    entry: 5,
    modalType: 0,
    editable: false,
    itemEditable: false,
    submitError: ''
  }

  getEndPoints = (params) => {
    return new Promise((resolve, reject) => {
      switch (params) {
        case 'Trades':
          resolve(
            this.setState({
              get: 'misc/gettrades/',
              save: 'misc/savetrade/',
              delete: 'misc/deletetrade/'
            })
          )
          break
        case 'Companies':
          resolve(
            this.setState({
              get: 'companies/getcompanies/',
              save: 'companies/savecompanies/',
              delete: 'companies/deleteCompany/'
            })
          )
          break;
        case 'Items':
          resolve(
            this.setState({
              get: 'misc/getitems/',
              save: 'misc/saveitems/',
              savePicture: 'misc/saveitempicture/',
              delete: 'misc/deleteitem/'
            })
          )
          break;
        case 'BuildingType':
          resolve(
            this.setState({
              get: 'projects/getBuildingTypes/',
              save: 'projects/saveBuildingTypes/',
              delete: 'projects/deleteBuildingType/'
            })
          )
          break;
        case 'Roles':
          resolve(
            this.setState({
              get: 'misc/getRoles/',
              save: 'misc/saveRole/',
              delete: 'misc/deleteRole/'
            })
          )
          break;
        case 'Manufacturers':
          resolve(
            this.setState({
              get: 'misc/getmanufacturers/',
              save: 'misc/savemanufacturers/',
              delete: 'misc/deleteManufacturer/'
            })
          )
          break;
        case 'TicketTypes':
          resolve(
            this.setState({
              get: 'tickets/gettickettypes/',
              save: 'tickets/saveticketTypes/',
              delete: 'tickets/deleteTicketType/'
            })
          )
          break;
        case 'Projects':
          resolve(
            this.setState({
              get: 'projects/GetProjects/',
              save: 'projects/saveProjects/',
              delete: 'projects/deleteproject/'
            })
          )
          break;
        case 'CompanyAddress':
          let getActiveComapnyId = this.state.activeComapnyId
          console.log("getActiveComapnyId --------->>>>>>>>>>>>>>>", getActiveComapnyId)
          resolve(
            this.props.props.history.push('/Companies')
          )
          break;
        default:
          resolve(
            this.props.props.history.push('/Companies')
            // this.setState({
            //   selectedTab: 'Companies',
            //   get: 'companies/getcompanies/',
            //   save: 'companies/savecompanies/',
            //   delete: 'companies/deleteCompany/'
            // })
          )
          break
      }
    })
  }

  initializeEditData = () =>{
    this.setState({
      editData: {
        Id: 0,
        RecordStatusId: 1,
        DateCreated: new Date(),
        activeComapnyId: 2
      }
    })
  }

  getData = () => {
    this.showLoader(true)
    let arr = []
    let contArr = []
    axios.get(`${API_URL}${this.state.get}${this.state.entry}/${this.state.activePageNumber}`)
      .then(res => {
        console.log("comnpany url -------------->>>", `${API_URL}${this.state.get}${this.state.entry}/${this.state.activePageNumber}`)
        this.showLoader(false)
        if (res.data.data[0] !== undefined) {
          let arr = []
          let header = Object.keys(res.data.data[0])
          header.map(data => {
            arr.push(data)
            this.setState({
              tableHeading: arr
            })
          })
        }
        this.setState({
          data: res.data.data,
          data_length: res.data.message,
          show: false
        })
        axios.get(`${API_URL}projects/getBuildingTypes/100/1`)
          .then(res => {
            res.data.data.map(data => {
              arr.push(data.name)
            })
            this.setState({
              BuildingTypeData: arr
            })
          })

        axios.get(`${API_URL}companies/getcompanies/100/1`)
          .then(res => {
            res.data.data.map(data => {
              contArr.push(data.name)
            })
            this.setState({
              contractorData: contArr
            })
          })
      })
  }

  componentDidMount() {
    this.getEndPoints(this.props.props.match.params.id)
      .then(res => {
        this.getData()
      })
  }

  setIsPagination = () => {
    setTimeout(() => {
      this.setState({
        isPagination: true
      })
    }, 10);
  }

  componentWillReceiveProps(param) {
    try {
      if (param.props.match.params.id !== this.state.selectedTab) {
        this.setState({
          isPagination: false,
          activePageNumber: 1
        })
        this.setIsPagination()
      }
    }
    catch (err) {
      console.log("catch err  --->", err)
      return
    }
    let params = param.props.match.params.id
    if (params === this.state.selectedTab) {
      return;
    } else {
      this.setState({
        selectedTab: params
      })
      this.getEndPoints(params)
        .then(res => {
          this.getData()
        })
    }
  }

  handlleEntryChange = (e) => {
    this.showLoader(true)
    let recordPerPage = e.target.value
    let pageNumber = this.state.activePageNumber
    axios.get(`${API_URL}${this.state.get}${recordPerPage}/${pageNumber}`)
      .then(res => {
        this.showLoader(false)
        this.setState({
          data: res.data.data
        })
      })
    this.setState({
      entry: e.target.value
    })
  }

  deleteRow = () => {
    this.showLoader(true)
    let index = this.state.deleteData.index
    let recordId = this.state.deleteData.recordId
    let userId = this.state.deleteData.userId
    try {
      axios.delete(`${API_URL}${this.state.delete}${recordId}/${userId}`)
        .then(
          (res) => {
            console.log("del ressss  ------------>", res.data.message)
            if (res.data.message == 'This company record is used in a related table.The record will not be deleted, it will be marked as Not Visible!') {
              this.setState({
                errors: { 'message': 'Can not delete this item' }
              })
              this.showLoader(false)
              return
            }
            this.showLoader(false)
            this.state.data.splice(index, 1);
            this.getData()
          },
          (error) => {
            console.log("del error  ------------>", error)
            this.showLoader(false)
            if (error.response.data.status == false) {
              this.setState({
                errors: { 'message': 'Can not delete this item' }
              })
            }
          }
        );
    } catch (err) {
      console.log("del catch -=>", err)
    }
  }

  deleteItemImage = (val, itemId) => {
    this.showLoader(true)
    axios.delete(`${API_URL}misc/deleteitempicture/${val.id}/${val.createdBy}`)
      .then(res => {
        axios.get(`${API_URL}misc/getitempictures/${itemId}`)
          .then(res => {
            this.showLoader(false)
            this.setState({
              itemImages: res.data.data,
            })
          })
      })
  }

  getPageNumber = (val) => {
    this.showLoader(true)
    axios.get(`${API_URL}${this.state.get}${this.state.entry}/${val}`)
      .then(res => {
        this.showLoader(false)
        this.setState({
          data: res.data.data,
          activePageNumber: val
        })
      })
  }

  editRecord = () => {
    this.setState({
      editable: !this.state.editable,
    })
  }


  editRow = () => {
    this.setState({
      itemEditable: true
    })
  }

  handleShow = (editStaus, data, index, modal_type, i) => {
    this.setState({
      errors: {},
      //editData : {}
    })
    if (modal_type == 0) {
      console.log("editStaus - --------=-=-=-=-========>>>", editStaus)
      if (editStaus == 0) {
        this.initializeEditData()
        this.setState({
          show: true,
          // editData: {
          // },
          selectedValue: 1,
          modalType: 0
        })
      }
      else {
        this.setState({
          editData: {
            Id: data.id,
            Name: data.name,
            RecordStatusId: data.recordStatusId,
            DateCreated: data.dateCreated,
            QbId: data.qbId,
            WebSite: data.webSite,
            Phone: data.phone,
            IndustryId: data.industryId,
            Address: data.address,
            City: data.city,
            Province: data.province,
            PostalCode: data.postalCode,
            Country: data.country,
            StartDate: data.startDate,
            EndDate: data.endDate,
            BuildingTypeId: data.buildingTypeId,
            ContractorId: data.contractorId,
            Active: data.active,
            Email: data.email,
            CompanyId: data.companyId,
            ContactPerson: data.contactPerson,
            Type: data.type,
          },
          selectedValue: index,
          show: true,
          modalType: 0
        })
      }
    } else {
      this.setState({
        show: true,
        modalType: 1,
        deleteData: {
          index: i,
          recordId: data.id,
          userId: data.createdBy
        }
      })
    }
  }

  handleShowItemScreen = (i, data, index) => {
    this.showLoader(true)
    axios.get(`${API_URL}misc/getitempictures/${data.id}`)
      .then(res => {
        this.showLoader(false)
        this.setState({
          ItemModalshow: true,
          itemData: data,
          itemImages: res.data.data,
          itemIndex: index,
          itemImageId: data.id
        })
      })
  }

  handleItemModalClose = () => {
    this.getData()
    this.setState({
      ItemModalshow: false,
    })
  }

  handleClose = () => {
    this.setState({
      show: false,
      ItemModalshow: false,
      editData: {
        ...this.state.editData,
        Name: '',
      },
      submitError: ''
    })
  }

  handleFieldChange = (e) => {
    console.log("name  ------------>>>", e.target.name)
    console.log("val  ------------>>>", e.target.value)
    let Name = e.target.name
    this.setState({
      editData: {
        ...this.state.editData,
        [Name]: e.target.value
      }
    })
  }


  handleRecordChange = (index, e) => {
    const updatedArray = [...this.state.data];
    updatedArray[index].recordStatusId = e.target.value;
    if (MultiEditarray[index] == undefined) {
      MultiEditarray.push(updatedArray[index])
    } else {
      MultiEditarray[index] = updatedArray[index]
    }
    this.setState({
      ...this.state.updatedData,
      updatedData: MultiEditarray,
    });
  }

  handleNameChange = (index, e) => {
    let name = (e.target.name).charAt(0).toLowerCase() + (e.target.name).slice(1)
    let val
    if (name == 'active') {
      var isTrueSet = (e.target.value === 'true');
      val = isTrueSet
    } else {
      val = e.target.value
    }
    const updatedArray = [...this.state.data];
    console.log("update aaary ->", updatedArray)
    updatedArray[index][name] = val;
    if (MultiEditarray[index] == undefined) {
      MultiEditarray.push(updatedArray[index])
    } else {
      MultiEditarray[index] = updatedArray[index]
    }
    this.setState({
      ...this.state.updatedData,
      updatedData: MultiEditarray,
    });
  }

  saveImage = (img, itemId, main) => {
    this.showLoader(true)
    return new Promise((resolve) => {
      if (!img.length) {
        resolve()
      }
      if (img.length) {
        let formData = new FormData()
        formData.append("Id", 0);
        formData.append("FileUrl", img[0]);
        formData.append("ItemId", itemId);
        formData.append("Main", main);
        formData.append("CreatedBy", 1);
        formData.append("ModifiedBy", 1);
        axios.post(`${API_URL}${this.state.savePicture}`, formData)
          .then(res => {
            axios.get(`${API_URL}misc/getitempictures/${itemId}`)
              .then(res => {
                this.showLoader(false)
                this.setState({
                  itemImages: res.data.data,
                })
                resolve()
              })
          })
      } else {
        resolve()
      }
    })
  }

  handleSubmit = (img) => {
    this.showLoader(true)
    const error = microValidator.validate(validationSchema, this.state.editData)
    // if (!is.empty(error)) {
    //   console.log("eeeeeeerrrrrrr", error)
    //   this.setState({
    //     errors: error
    //   })
    //   return
    // }
    if (this.state.selectedTab === 'Trades' || this.state.selectedTab === 'Roles') {
      console.log("this.state.editData.Id ->", this.state.editData.Id)
      payload = {
        Id: this.state.editData.Id,
        Name: this.state.editData.Name,
        RecordStatusId: this.state.editData.RecordStatusId,
        DateCreated: this.state.editData.DateCreated,
        CreatedBy: 1,
        ModifiedBy: 1,
        DateModified: new Date()
      }
    } else {
      payload = {
        content: JSON.stringify([{
          Id: this.state.editData.Id,
          Name: this.state.editData.Name,
          RecordStatusId: this.state.editData.RecordStatusId,
          CreatedBy: 1,
          ModifiedBy: 1,
          QbId: parseInt(this.state.editData.QbId),
          WebSite: this.state.editData.WebSite,
          Phone: this.state.editData.Phone,
          IndustryId: parseInt(this.state.editData.IndustryId),
          Active: this.state.editData.Active,
          Address: this.state.editData.Address,
          City: this.state.editData.City,
          Province: this.state.editData.Province,
          PostalCode: this.state.editData.PostalCode,
          Country: this.state.editData.Country,
          StartDate: this.state.editData.StartDate,
          EndDate: this.state.editData.EndDate,
          BuildingTypeId: this.state.editData.BuildingTypeId,
          ContractorId: this.state.editData.ContractorId,
          CompanyId: this.state.editData.CompanyId,
          Type: this.state.editData.Type,
          ContactPerson: this.state.editData.ContactPerson,
          Email: this.state.editData.Email

        }])
      }
    }

    console.log("payload --- ->", payload)
    axios.post(`${API_URL}${this.state.save}`, payload)
      .then(res => {
        let id = res.data.data || this.state.editData.Id
        this.showLoader(false)
        this.saveImage(img, id, true)
          .then(response => {
            this.setState([]);
            if (res.data.message === 'OK') {
              this.setState({
                //editData: {},
                submitError: ('')
              })
              this.getData()
            } else {
              this.showLoader(false)
              this.setState({
                submitError: 'A record with the same name already exists'
              })
            }
          })
      })
      .catch(err => console.log("api errorrrrrrrrr    ------>", err))
  }

  saveMultipe = () => {
    this.showLoader(true)
    if (this.state.selectedTab === 'Trades' || this.state.selectedTab === 'Roles') {
      for (let i = 0; i < MultiEditarray.length; i++) {
        let payload = {
          Id: MultiEditarray[i].id,
          Name: MultiEditarray[i].name,
          RecordStatusId: MultiEditarray[i].recordStatusId,
          DateCreated: MultiEditarray[i].dateCreated,
          CreatedBy: MultiEditarray[i].createdBy,
          ModifiedBy: MultiEditarray[i].modifiedBy,
          DateModified: new Date()
        }
        axios.post(`${API_URL}${this.state.save}`, payload)
          .then(res => {
            this.showLoader(false)
            this.handleItemModalClose()
            axios.get(`${API_URL}${this.state.get}${this.state.entry}/${this.state.activePageNumber}`)
              .then(res => {
                this.setState({
                  data: res.data.data
                })
              })
            this.setState({
              editable: false,
              itemEditable: false
            })
          })
      }
    }
    else {
      let tempArray = []
      for (let i = 0; i < MultiEditarray.length; i++) {
        let data = {
          Id: MultiEditarray[i].id,
          Name: MultiEditarray[i].name,
          RecordStatusId: MultiEditarray[i].recordStatusId,
          DateCreated: MultiEditarray[i].dateCreated,
          CreatedBy: MultiEditarray[i].createdBy,
          ModifiedBy: MultiEditarray[i].modifiedBy,
          DateModified: new Date(),
          QbId: MultiEditarray[i].qbId,
          WebSite: MultiEditarray[i].webSite,
          Phone: MultiEditarray[i].phone,
          IndustryId: MultiEditarray[i].industryId,
          Type: MultiEditarray[i].type,
          Address: MultiEditarray[i].address,
          City: MultiEditarray[i].city,
          PostalCode: MultiEditarray[i].postalCode,
          Province: MultiEditarray[i].province,
          Country: MultiEditarray[i].country,
          ContactPerson: MultiEditarray[i].contactPerson,
          Phone: MultiEditarray[i].phone,
          Email: MultiEditarray[i].email,
          CompanyId: MultiEditarray[i].companyId,
          Active: MultiEditarray[i].active,
        }
        tempArray.push(data)
      }
      payload = {
        content: JSON.stringify(tempArray)
      }
      console.log("payload ---->", payload)
      axios.post(`${API_URL}${this.state.save}`, payload)
        .then(res => {
          this.showLoader(false)
          this.handleItemModalClose()
          axios.get(`${API_URL}${this.state.get}${this.state.entry}/${this.state.activePageNumber}`)
            .then(res => {
              this.showLoader(false)
              this.setState({
                data: res.data.data
              })
            })
          this.setState({
            editable: false,
            itemEditable: false
          })
        })
    }
  }

  onSearch = (e) => {
    this.showLoader(true)
    console.log("search url ---------------------===>>", `${API_URL}${this.state.get}${this.state.entry}/${this.state.activePageNumber}/${e.target.value}`)
    axios.get(`${API_URL}${this.state.get}${this.state.entry}/${this.state.activePageNumber}/${e.target.value}`)
      .then(res => {
        this.showLoader(false)
        this.setState({
          data: res.data.data
        })
      })
  }

  getCompanyAddress = (id) => {
    console.log("getCompanyAddress  -- >", id)
    return new Promise((resolve, reject) => {
      resolve(
        this.setState({
          get: `companies/getcompanyaddresses/${id}/`,
          save: 'companies/savecompanyAddresses/',
          delete: 'companies/deleteCompanyAddress/',
          selectedTab: 'CompanyAddress',
          activeComapnyId: id
        })
      )
    })
  }

  handleTdClick = (selected, data) => {
    this.showLoader(true)
    if (selected === 'Companies') {
      this.getCompanyAddress(data.id)
        .then(res => {
          this.props.props.history.push('/CompanyAddress')
          axios.get(`${API_URL}${this.state.get}${this.state.entry}/${this.state.activePageNumber}`)
            .then(res => {
              this.showLoader(false)
              if (res.data.data[0] !== undefined) {
                let arr = []
                let header = Object.keys(res.data.data[0])
                header.map(data => {
                  arr.push(data)
                  this.setState({
                    tableHeading: arr
                  })
                })
              }
              this.setState({
                data: res.data.data,
                data_length: res.data.message,
                show: false
              })
            })
        })
    }
  }

  showLoader = (isLoader) => {
    this.props.parentCallback(isLoader);
  }

  render() {
    console.log("activeComapnyId ----- ----====>>", this.state.activeComapnyId)
    // let pagination_data = this.state.data.slice((this.state.activePageNumber-1) * this.state.entry , this.state.activePageNumber * this.state.entry);
    return (
      <div className="container-fluid">
        <Modal state={this.state} show={this.state.show} modalType={this.state.modalType} handleClose={this.handleClose} handleFieldChange={this.handleFieldChange}
          handleRecordChange={this.handleEachRecordChange} editData={this.state.editData} deleteRow={this.deleteRow} handleSubmit={this.handleSubmit} errors={this.state.errors}
          values={this.state.values} selectedValue={this.state.selectedValue} submitError={this.state.submitError} />

        <ItemPreviewModal handleNameChange={this.handleNameChange} saveMultipe={this.saveMultipe} editRow={this.editRow}
          handleRecordChange={this.handleRecordChange} handleItemModalClose={this.handleItemModalClose} state={this.state}
          saveImage={this.saveImage} deleteItemImage={this.deleteItemImage} />
        <h1 className="h3 mb-2 text-gray-800 d-inline-block page_title trade_text">{this.state.selectedTab}</h1>
        {
          this.state.editable ?
            <button onClick={() => this.saveMultipe()} className="btn btn-primary float-right mb-3 new_records">Save Records</button>
            :
            <button onClick={() => this.handleShow(0, this.state.editData, null, 0)} className="btn btn-primary float-right mb-3 new_records">New Records</button>
        }
        <button onClick={this.editRecord} className="btn btn-success float-right mb-3 mr-1 edit_button">{this.state.editable ? 'Cancel' : 'Edit'}</button>
        <div className="card shadow mb-4 w-100">
          <div className="card-header py-3">
            <h6 className="m-0 font-weight-bold text-primary trade_text">{this.state.selectedTab} Records</h6>
          </div>
          <div className="card-body">
            <div className='row bottomValues'>
              <div>
                <div className="dataTables_length" id="dataTable_length">
                  <label className="label_entry">
                    Show {'\u00A0'}
                    <select onChange={this.handlleEntryChange} name="dataTable_length" aria-controls="dataTable" className="custom-select custom-select-sm form-control form-control-sm">
                      <option value="5">5</option>
                      <option value="10">10</option>
                      <option value="25">25</option>
                      <option value="50">50</option>
                      <option value="100">100</option>
                    </select>
                    {'\u00A0'} entries
                  </label>
                </div>
              </div>
              <div>
                <div id="dataTable_filter" className="dataTables_filter">
                  <label className="label_entry">
                    Search: {'\u00A0'}
                    <input defaultValue={this.props.searchData} onChange={(e) => this.onSearch(e)} type="search" className="form-control form-control-sm" placeholder="Search Name" aria-controls="dataTable" />
                  </label>
                </div>
              </div>
            </div>
            <div className="table-responsive">
              <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0" >
                <thead>
                  {
                    <tr>
                      {this.state.tableHeading.map((key, index) => {
                        if (key === 'recordStatusId' || key === 'modifiedBy' || key === 'createdBy' || key === 'imageUrl') {
                          return
                        }
                        return (
                          <Fragment>
                            <th key={index}>{key.charAt(0).toUpperCase() + key.slice(1)}</th>
                          </Fragment>
                        )
                      })}
                      <th>Action</th>
                    </tr>
                  }

                </thead>
                <tbody className="tableContainer">
                  {
                    this.state.data.map((data, index) => {
                      return (
                        <tr key={index} >
                          {Object.keys(data).map(k => {
                            switch (k) {
                              case 'thumbImageUrl':
                                return (
                                  <td>{<img src={data[k]} className='item-preview' alt='img' />}</td>
                                )
                              case 'imageUrl':
                                return
                              case 'recordStatusId':
                                return
                              case 'modifiedBy':
                                return
                              case 'createdBy':
                                return
                              case 'dateCreated':
                                return <td><div>{moment(data[k]).format('L')}</div></td>
                              case 'dateModified':
                                return <td><div>{moment(data[k]).format('L')}</div></td>
                              case 'startDate':
                                return <td><div>{moment(data[k]).format('L')}</div></td>
                              case 'endDate':
                                return <td><div>{moment(data[k]).format('L')}</div></td>

                              case 'active':
                                return (
                                  <td className="tableIdCon">
                                    {this.state.editable ?
                                      <select name="Active" value={this.state.value} onChange={(e) => { this.handleNameChange(index, e) }}
                                        className="custom-select w-100">
                                        {
                                          this.state.ActiveVal.map((val, i) => {
                                            if (val.toString() == data[k].toString()) {
                                              return (<option selected={val.toString()}>{data[k].toString()}</option>)
                                            }
                                            return (<option value={val}>{val.toString()}</option>)
                                          })
                                        }
                                      </select>
                                      :
                                      <Fragment>
                                        {data[k].toString()}
                                      </Fragment>
                                    }
                                  </td>
                                )

                              case 'name':
                                return (
                                  <td className="tableIdCon">
                                    <div className="nameContent companyContent" >
                                      <input name="Name" className={this.state.editable ? '' : 'input_cell'} type='text' value={data.name}
                                        onChange={(e) => this.handleNameChange(index, e)}
                                        disabled={(this.state.editable) ? "" : "disabled"} />
                                    </div>
                                  </td>
                                )

                              case 'qbId':
                                return (
                                  <td className="tableIdCon">
                                    <div className="nameContent">
                                      <input name="QbId" className={this.state.editable ? '' : 'input_cell'} type='text' value={data.qbId}
                                        onChange={(e) => this.handleNameChange(index, e)}
                                        disabled={(this.state.editable) ? "" : "disabled"} />
                                    </div>
                                  </td>
                                )

                              case 'webSite':
                                return (
                                  <td className="tableIdCon">
                                    <div className="nameContent">
                                      <input name="WebSite" className={this.state.editable ? '' : 'input_cell'} type='text' value={data[k]}
                                        onChange={(e) => this.handleNameChange(index, e)}
                                        disabled={(this.state.editable) ? "" : "disabled"} />
                                    </div>
                                  </td>
                                )

                              case 'phone':
                                return (
                                  <td className="tableIdCon">
                                    <div className="nameContent">
                                      <input name="Phone" className={this.state.editable ? '' : 'input_cell'} type='text' value={data[k]}
                                        onChange={(e) => this.handleNameChange(index, e)}
                                        disabled={(this.state.editable) ? "" : "disabled"} />
                                    </div>
                                  </td>
                                )

                              case 'industryId':
                                return (
                                  <td className="tableIdCon">
                                    <div className="nameContent">
                                      <input name="IndustryId" className={this.state.editable ? '' : 'input_cell'} type='text' value={data[k]}
                                        onChange={(e) => this.handleNameChange(index, e)}
                                        disabled={(this.state.editable) ? "" : "disabled"} />
                                    </div>
                                  </td>
                                )

                              case 'address':
                                return (
                                  <td className="tableIdCon">
                                    <div className="nameContent">
                                      <input name="Address" className={this.state.editable ? '' : 'input_cell'} type='text' value={data[k]}
                                        onChange={(e) => this.handleNameChange(index, e)}
                                        disabled={(this.state.editable) ? "" : "disabled"} />
                                    </div>
                                  </td>
                                )


                              case 'city':
                                return (
                                  <td className="tableIdCon">
                                    <div className="nameContent">
                                      <input name="City" className={this.state.editable ? '' : 'input_cell'} type='text' value={data[k]}
                                        onChange={(e) => this.handleNameChange(index, e)}
                                        disabled={(this.state.editable) ? "" : "disabled"} />
                                    </div>
                                  </td>
                                )

                              case 'province':
                                return (
                                  <td className="tableIdCon">
                                    <div className="nameContent">
                                      <input name="Province" className={this.state.editable ? '' : 'input_cell'} type='text' value={data[k]}
                                        onChange={(e) => this.handleNameChange(index, e)}
                                        disabled={(this.state.editable) ? "" : "disabled"} />
                                    </div>
                                  </td>
                                )

                              case 'postalCode':
                                return (
                                  <td className="tableIdCon">
                                    <div className="nameContent">
                                      <input name="PostalCode" className={this.state.editable ? '' : 'input_cell'} type='text' value={data[k]}
                                        onChange={(e) => this.handleNameChange(index, e)}
                                        disabled={(this.state.editable) ? "" : "disabled"} />
                                    </div>
                                  </td>
                                )

                              case 'country':
                                return (
                                  <td className="tableIdCon">
                                    <div className="nameContent">
                                      <input name="Country" className={this.state.editable ? '' : 'input_cell'} type='text' value={data[k]}
                                        onChange={(e) => this.handleNameChange(index, e)}
                                        disabled={(this.state.editable) ? "" : "disabled"} />
                                    </div>
                                  </td>
                                )

                              case 'contactPerson':
                                return (
                                  <td className="tableIdCon">
                                    <div className="nameContent">
                                      <input name="ContactPerson" className={this.state.editable ? '' : 'input_cell'} type='text' value={data[k]}
                                        onChange={(e) => this.handleNameChange(index, e)}
                                        disabled={(this.state.editable) ? "" : "disabled"} />
                                    </div>
                                  </td>
                                )

                              case 'phone':
                                return (
                                  <td className="tableIdCon">
                                    <div className="nameContent">
                                      <input name="Phone" className={this.state.editable ? '' : 'input_cell'} type='text' value={data[k]}
                                        onChange={(e) => this.handleNameChange(index, e)}
                                        disabled={(this.state.editable) ? "" : "disabled"} />
                                    </div>
                                  </td>
                                )

                              case 'email':
                                return (
                                  <td className="tableIdCon">
                                    <div className="nameContent">
                                      <input name="email" className={this.state.editable ? '' : 'input_cell'} type='text' value={data[k]}
                                        onChange={(e) => this.handleNameChange(index, e)}
                                        disabled={(this.state.editable) ? "" : "disabled"} />
                                    </div>
                                  </td>
                                )

                              case 'industryName':
                                return (
                                  <td className="tableIdCon">
                                    <Typeahead
                                      {...this.state}
                                      bodyContainer={true}
                                      id="bulding-type"
                                      options={this.state.BuildingTypeData}
                                      placeholder={this.state.BuildingTypeData[0]}
                                    />
                                  </td>
                                )
                              case 'buildingTypeName':
                                return (
                                  <td>
                                    <Typeahead
                                      {...this.state}
                                      bodyContainer={true}
                                      id="bulding-type"
                                      // onChange={selected => this.setState({ selected })}
                                      options={this.state.BuildingTypeData}
                                      placeholder={this.state.BuildingTypeData[0]}
                                    />
                                  </td>
                                )

                              case 'contractorName':
                                return (
                                  <td>
                                    <Typeahead
                                      {...this.state}
                                      bodyContainer={true}
                                      id="bulding-type"
                                      options={this.state.contractorData}
                                      placeholder={this.state.contractorData[0]}
                                    />
                                  </td>
                                )

                              case 'recordStatus':
                                return (
                                  <td className="tableIdCon">
                                    {this.state.editable ?
                                      <select name="RecordStatusId" value={this.state.value} onChange={(e) => { this.handleRecordChange(index, e) }}
                                        className="custom-select w-100">
                                        {
                                          this.state.values.map((val, i) => {
                                            if (val.id == data.recordStatusId) {
                                              return (<option selected={val.id}>{val.name}</option>)
                                            }
                                            return (<option value={val.id}>{val.name}</option>)

                                          })
                                        }
                                      </select>
                                      :
                                      <Fragment>
                                        {data.recordStatus}
                                      </Fragment>
                                    }
                                  </td>
                                )

                              default:
                                return (
                                  <Fragment>
                                    <td key={data}><div
                                      value={k}>{data[k]}</div></td>
                                  </Fragment>
                                );
                            }
                          })}
                          <td>
                            <div className="actionContainer">
                              {this.state.editable ? '' : <span><i onClick={this.state.selectedTab === 'Items' ?
                                () => this.handleShowItemScreen(1, data, index) :
                                () => this.handleShow(1, data, data.recordStatusId, 0, index)} className="fas fa-pen text-warning"></i> &nbsp; </span>}
                              <span><i onClick={() => this.handleShow(1, data, data.recordStatusId, 1, index)} className="fas fa-trash-alt text-danger"></i> </span>
                              {this.state.selectedTab === 'Companies' ?
                                <span><i onClick={(e) => this.handleTdClick(this.state.selectedTab, data)} className="fas fa-address-card text-success"></i> </span>
                                : null}
                            </div>
                          </td>
                        </tr>

                      );
                    })
                  }
                </tbody>
              </table>
            </div>
            <div className='row bottomValues'>
              <div className="dataTables_info" id="dataTable_info" role="status" aria-live="polite">Showing {(this.state.activePageNumber * this.state.entry) - this.state.entry} to {this.state.activePageNumber * this.state.entry} of {this.state.data_length} entries</div>
              {this.state.isPagination ? <Pagination totalItemsCount={this.state.data_length} sendPageNumber={this.getPageNumber} entry={this.state.entry} /> : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Table;