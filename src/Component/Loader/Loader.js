import React from 'react'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner'
import './Loader.css'
const Loading = () => {
    return (
        <div className="loader">   
            <Loader
                className="loading-spinner"
                type="TailSpin"
                color="#fdebbd"
                height={50}
                width={50}
            />
        </div>
    )
}
export default Loading
