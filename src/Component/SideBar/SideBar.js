import React, { Component } from 'react';
import '../../App.css';
import './SideBar.css';
import '../../Css/admin.css';
import '../../Css/bootstrap4.min.css';
import 'font-awesome/css/font-awesome.min.css';

class SideBar extends Component {

    state = {
        selectesTab: null
    }

    componentDidMount() {
        this.setState({
            selectesTab: `${this.props.props.match ? this.props.props.match.params.id : 'Companies'}`
        })
    }

    handleTabChange = (tab) => {
        this.props.props.history.push(tab)
        this.setState({
            selectesTab: tab
        })
    }

    render() {
        return (
            <div>
                <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion content" id="accordionSidebar" >
                    <a className="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                        <div className="sidebar-brand-icon ">
                            <i className="fas fa-database"></i>
                        </div>
                        <div className="sidebar-brand-text mx-3">Web App </div>
                    </a>

                    <hr className="sidebar-divider my-0" />
                    <li className="nav-item">
                        <a className="nav-link" href="#">
                            <i className="fas fa-fw fa-tachometer-alt"></i>
                            <span className="side-menu">Dashboard</span></a>
                    </li>

                    <hr className="sidebar-divider" />

                    <li className={this.state.selectesTab === 'BuildingType' ? 'nav-item active' : 'nav-item'}>
                        <div className="nav-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                            <i className="fas fa-building"></i>
                            <span className="side-menu" onClick={() => this.handleTabChange('BuildingType')} >BuildingType</span>
                            {/* <i className="fas fa-sort-down float-right"></i> */}
                        </div>
                        {/* <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                                <div className="bg-white py-2 collapse-inner rounded">
                                    <a className="collapse-item" href="buttons.html">Dropdown 1</a>
                                    <a className="collapse-item" href="cards.html">Dropdown 2</a>
                                </div>
                            </div> */}
                    </li>

                    <li className={this.state.selectesTab === 'Companies' ? 'nav-item active' : 'nav-item'}>
                        <div className="nav-link collapsed" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                            <i className="fas fa-warehouse"></i>
                            <span className="side-menu" onClick={() => this.handleTabChange('Companies')}>Companies</span>
                        </div>
                    </li>

                    <li className={this.state.selectesTab === 'Roles' ? 'nav-item active' : 'nav-item'}>
                        <div className="nav-link collapsed" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                            <i className="fas fa-user-tag"></i>
                            <span className="side-menu" onClick={() => this.handleTabChange('Roles')}>Roles</span>
                        </div>
                    </li>


                    <li className={this.state.selectesTab === 'Items' ? 'nav-item active' : 'nav-item'}>
                        <div className="nav-link collapsed" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                            <i className="fas fa-business-time"></i>
                            <span className="side-menu" onClick={() => this.handleTabChange('Items')}>Items</span>
                        </div>
                    </li>



                    <li className={this.state.selectesTab === 'Manufacturers' ? 'nav-item active' : 'nav-item'}>
                        <div className="nav-link collapsed" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                            <i className="fas fa-hotel"></i>
                            <span className="side-menu" onClick={() => this.handleTabChange('Manufacturers')}>Manufacturers</span>
                        </div>
                    </li>


                    {/* <li className={this.state.selectesTab === 'Products' ? 'nav-item active' : 'nav-item'}>
                        <div className="nav-link collapsed" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                            <i className="fas fa-bookmark"></i>
                            <span className="side-menu" >Products</span>
                        </div>
                    </li>


                    <li className={this.state.selectesTab === 'Product Types' ? 'nav-item active' : 'nav-item'}>
                        <div className="nav-link collapsed" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                            <i className="fas fa-bookmark"></i>
                            <span className="side-menu">Product Types</span>
                        </div>
                    </li> */}


                    <li className={this.state.selectesTab === 'Projects' ? 'nav-item active' : 'nav-item'}>
                        <div className="nav-link collapsed" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                            <i className="fas fa-server"></i>
                            <span className="side-menu" onClick={() => this.handleTabChange('Projects')}>Projects</span>
                        </div>
                    </li>

                    <li className={this.state.selectesTab === 'TicketTypes' ? 'nav-item active' : 'nav-item'}>
                        <div className="nav-link collapsed" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                            <i className="fas fa-bookmark"></i>
                            <span className="side-menu" onClick={() => this.handleTabChange('TicketTypes')}>Ticket Types</span>
                        </div>
                    </li>


                    <li className={this.state.selectesTab === 'Questions' ? 'nav-item active' : 'nav-item'}>
                        <div className="nav-link collapsed" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                            <i className="fas fa-question"></i>
                            <span className="side-menu" >Questions</span>
                        </div>
                    </li>


                    <li className={this.state.selectesTab === 'Trades' ? 'nav-item active' : 'nav-item'}>
                        <div className="nav-link">
                            <i className="fas fa-fw fa-table"></i>
                            <span className="side-menu" onClick={() => this.handleTabChange('Trades')}>Trades</span></div>
                    </li>
                </ul>
            </div>
        );
    }
}

export default SideBar;